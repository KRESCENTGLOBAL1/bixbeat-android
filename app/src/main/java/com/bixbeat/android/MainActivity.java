package com.bixbeat.android;



import android.graphics.Color;

import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.graphics.Bitmap;
import android.content.Intent;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.app.Activity;


import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.TimeUnit;

import im.delight.android.webview.AdvancedWebView;
import mediaplayer.MediaPlayerHolder;
import mediaplayer.PlaybackInfoListener;
import mediaplayer.PlayerAdapter;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity implements AdvancedWebView.Listener {

    private int duration;
    public PlayerAdapter mPlayerAdapter;
    public static final int MEDIA_RES_ID = R.raw.jazz_in_paris;
    private TextView tvDuration;

    private static final String TEST_PAGE_URL = "https://www.bixbeat.com/?platform=android";
    //private static final String TEST_PAGE_URL ="https://bixbeat.azurewebsites.net?platform=android";
    //private static final String TEST_PAGE_URL ="https://bixbeat2beta.azurewebsites.net/index2.html?aaa=b";
    //private static final String TEST_PAGE_URL ="https://jsfiddle.net/tntomek/vc1k0eyb/";

    private static final int RC_GET_AUTH_CODE = 9003;

    private GoogleSignInClient mGoogleSignInClient;

    private AdvancedWebView mWebView;


    private TextView mTextDebug;
    private SeekBar mSeekbarAudio;
    private SeekBar mSeekbarAudio_min;
    private ScrollView mScrollContainer;

    private boolean mUserIsSeeking = false;
    private TextView currentPosition;

    Button mPlayBtn;
    Button mPauseBtn;
    int mCurrentPlaybackPosition;
    int mCurrentPlaybackPosition_min;
    TextView tvSongTitle;
    TextView tvArtist;
    TextView tvArtist2;
    LinearLayout minimizeBar;

    TextView tvSongTitleMin;
    TextView tvArtistMin;

    ImageView cover;

    LinearLayout player;
    LinearLayout minimized_player;

    Button mPlayBtn_min;
    Button mPauseBtn_min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title bar

        setContentView(R.layout.activity_main);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        mWebView.setListener(this, this);
        mWebView.setGeolocationEnabled(false);
        mWebView.setMixedContentAllowed(true);
        mWebView.setCookiesEnabled(true);
        mWebView.setThirdPartyCookiesEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        final BixWebAppInterface bbInt = new BixWebAppInterface(this, player);

        // WebChromeClient aaa;
        mWebView.addJavascriptInterface(bbInt, "Android");

        /*
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                // bbInt.onDownload("https://bixbeatmediaservice.streaming.mediaservices.windows.net/dfbb093d-b44d-406e-858e-093213e05825/20161126204735-eba9e1a4-b86f-4a0_AAC_192.mp4", "somefile/.mp4");
                // Toast.makeText(MainActivity.this, "Finished loading", Toast.LENGTH_SHORT).show();
            }

        });
        */
        /*
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                Toast.makeText(MainActivity.this, title, Toast.LENGTH_SHORT).show();
            }

        });
        */
        mWebView.addHttpHeader("Referrer", "https://bixbeat.com");
        mWebView.addHttpHeader("X-Requested-With", "android.bixbeat.com");

        mWebView.loadUrl(TEST_PAGE_URL, true);

        // Request only the user's ID token, which can be used to identify the
        // user securely to your backend. This will contain the user's basic
        // profile (name, profile picture URL, etc) so you should not need to
        // make an additional call to personalize your application.
        /*
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .requestProfile()
                .build();
        */

        String serverClientId = getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        initializeUI();
        //mPlayer. initializeSeekbar();
        //initializeSeekbar();
        initializePlaybackController();
    }

    public void initializeUI() {
        //mTextDebug = (TextView) findViewById(R.id.text_debug);

        /*final Button mPlayButton = (Button) findViewById(R.id.button_play);
        final Button mPauseButton = (Button) findViewById(R.id.button_pause);
        final Button mSeekForward = findViewById(R.id.button_seek_forward);
        final Button mRewind = findViewById(R.id.button_rewind);
*/
        mPlayBtn_min = findViewById(R.id.button_play_min);
        mPauseBtn_min = findViewById(R.id.button_pause_min);
        /*final Button seekForward_min = findViewById(R.id.button_seek_forward_min);
        final Button rewind_min = findViewById(R.id.button_rewind_min);
       */final Button close_min = findViewById(R.id.close_min);

        cover = findViewById(R.id.cover);
        /*
        cover.setImageResource(R.drawable.cover);
        */
        tvSongTitleMin = findViewById(R.id.song_title_min);
        tvArtistMin = findViewById(R.id.artist_min);
        /*
        tvSongTitle = findViewById(R.id.song_title);
        tvArtist = findViewById(R.id.artist);
        tvArtist2 = findViewById(R.id.artist2);
        tvSongTitle.setText("Take Over");
        tvArtist.setText("Hayfan(Shatan Arewa)");
        tvArtist2.setText("Hayfan(Shatan Arewa)");
        tvSongTitleMin.setText("Take Over");
        tvArtistMin.setText("Hayfan(Shatan Arewa)");
        mPlayBtn = mPlayButton;
        mPauseBtn = mPauseButton;
        //Button mResetButton = (Button)findViewById(R.id.button_reset);
        mSeekbarAudio = (SeekBar) findViewById(R.id.seekbar_audio);
        mSeekbarAudio_min = (SeekBar) findViewById(R.id.seekbar_audio_min);
        //mScrollContainer = (ScrollView) findViewById(R.id.scroll_container);
        currentPosition = (TextView) findViewById(R.id.currentPosition);
        tvDuration = (TextView) findViewById(R.id.duration);
        minimizeBar = findViewById(R.id.minimize_bar);
        player = findViewById(R.id.player);*/
        minimized_player = findViewById(R.id.minimized_player);
/*
        mPauseButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.pause();
                        mPlayButton.setVisibility(View.VISIBLE);
                        mPauseButton.setVisibility(View.GONE);
                    }
                });
        mPlayButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.play();
                        mPauseButton.setVisibility(View.VISIBLE);
                        mPlayButton.setVisibility(View.GONE);
                    }
                });
        *//*mResetButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.reset();
                    }
                });*//*

        mSeekForward.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mCurrentPlaybackPosition < duration - 5000)
                            mPlayerAdapter.seekTo(mCurrentPlaybackPosition + 5000);
                        mSeekbarAudio.setProgress(mCurrentPlaybackPosition+5000);

                    }
                });

        mRewind.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mCurrentPlaybackPosition  > 5000)
                            mPlayerAdapter.seekTo(mCurrentPlaybackPosition - 5000);
                        mSeekbarAudio.setProgress(mCurrentPlaybackPosition - 5000);
                    }
                });

        minimizeBar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        player.setVisibility(View.GONE);
                        minimized_player.setVisibility(View.VISIBLE);
                        if(mPlayerAdapter.isPlaying()) {
                            mPlayBtn_min.setVisibility(View.GONE);
                            mPauseBtn_min.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            mPlayBtn_min.setVisibility(View.VISIBLE);
                            mPauseBtn_min.setVisibility(View.GONE);
                        }

                    }
                });
*/
        mPauseBtn_min.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.pause();
                        mPlayBtn_min.setVisibility(View.VISIBLE);
                        mPauseBtn_min.setVisibility(View.GONE);
                    }
                });
        mPlayBtn_min.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.play();
                        mPauseBtn_min.setVisibility(View.VISIBLE);
                        mPlayBtn_min.setVisibility(View.GONE);
                    }
                });
/*
        seekForward_min.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mCurrentPlaybackPosition_min < duration - 5000)
                            mPlayerAdapter.seekTo(mCurrentPlaybackPosition_min + 5000);
                        mSeekbarAudio_min.setProgress(mCurrentPlaybackPosition_min + 5000);
                    }
                });

        rewind_min.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mCurrentPlaybackPosition_min  > 5000)
                            mPlayerAdapter.seekTo(mCurrentPlaybackPosition_min - 5000);
                        mSeekbarAudio_min.setProgress(mCurrentPlaybackPosition_min - 5000);
                    }
                });
*/
        close_min.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPlayerAdapter.pause();
                        mPlayerAdapter.release();
                        minimized_player.setVisibility(View.GONE);
                    }
                });
    }

    public void toggle(String toShow, String toHide)
    {

    }

    public void initializeSeekbar() {
        mSeekbarAudio.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int userSelectedPosition = 0;

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        mUserIsSeeking = true;
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            userSelectedPosition = progress;
                        }
                        Long minutes = TimeUnit.MILLISECONDS.toMinutes(progress);
                        Long seconds = TimeUnit.MILLISECONDS.toSeconds(progress) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(progress));
                        String secondFormat;
                        if(seconds<10)
                        {
                            secondFormat = "0" + seconds.toString();
                        }
                        else
                            secondFormat = seconds + "";
                        currentPosition.setText(minutes + ":" + secondFormat);

                        mCurrentPlaybackPosition = progress;
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        mUserIsSeeking = false;
                        mPlayerAdapter.seekTo(userSelectedPosition);
                    }
                });

        mSeekbarAudio_min.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int userSelectedPosition = 0;

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        mUserIsSeeking = true;
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            userSelectedPosition = progress;
                        }


                        mCurrentPlaybackPosition_min = progress;
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        mUserIsSeeking = false;
                        mPlayerAdapter.seekTo(userSelectedPosition);
                    }
                });
    }

    // Method to share either text or URL.
    public void shareLink(String title, String text, String url) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        //share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, text);
        share.putExtra(Intent.EXTRA_TEXT, url);

        startActivity(Intent.createChooser(share, title));

        /*
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
        */
    }

    public void getAuthCode() {
        // Start the retrieval process for a server auth code.  If requested, ask for a refresh
        // token.  Otherwise, only get an access token if a refresh token has been previously
        // retrieved.  Getting a new access token for an existing grant does not require
        // user consent.
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
    }

   // @Override
    /*
    public void onActivityResult2(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mWebView.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_GET_AUTH_CODE) {
            // [START get_auth_code]
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            handleSignInResult(task);


            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                String authCode = account.getServerAuthCode();

                // Show signed-un UI
               //updateUI(account);

                // TODO(developer): send code to server and exchange for access/refresh/ID tokens
            } catch (ApiException e) {
               // Log.w(TAG, "Sign-in failed", e);
               // updateUI(null);
            }

            // [END get_auth_code]
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String authCode = account.getServerAuthCode();

            //Toast.makeText(MainActivity.this, authCode, Toast.LENGTH_SHORT).show();

            mWebView.loadUrl("javascript:window.Android.onLoginGoogleDone('"+ authCode +"')");

              //window["Android"].onLoginGoogleDone
            // Signed in successfully, show authenticated UI.
           // updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            //Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
            Toast.makeText(MainActivity.this, e.getStatusCode(), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }
*/

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        // mWebView.setVisibility(View.INVISIBLE);
    }

    /*
    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);

        progressBar.setVisibility(View.GONE);
    }
    */

    @Override
    public void onPageFinished(String url) {
        //mWebView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        Toast.makeText(MainActivity.this, "onPageError(errorCode = "+errorCode+",  description = "+description+",  failingUrl = "+failingUrl+")", Toast.LENGTH_SHORT).show();
    }



    /*
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        progressBar.setVisibility(View.VISIBLE);
        view.loadUrl(url);
        return true;

    }
    */

    /*
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (mWebView.isHostnameAllowed(url)) {
            if (BixWebView.this.mCustomWebViewClient == null || !BixWebView.this.mCustomWebViewClient.shouldOverrideUrlLoading(view, url)) {
                Map<String, String> headers = new HashMap();
                headers.put("Referer", "http://bixbeat.com");
                headers.put("X-Requested-With", "android.bixbeat.com");
                view.loadUrl(url, headers);
            }
        } else if (BixWebView.this.mListener != null) {
            BixWebView.this.mListener.onExternalPageRequest(url);
        }
        return true;
    }
*/

    /*
    TODO
    public static boolean handleDownload(final Context context, final String fromUrl, final String toFilename) {
        if (Build.VERSION.SDK_INT < 9) {
            throw new RuntimeException("Method requires API level 9 or above");
        }

        final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fromUrl));
        if (Build.VERSION.SDK_INT >= 11) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, toFilename);

        final DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        try {
            try {
                dm.enqueue(request);
            }
            catch (SecurityException e) {
                if (Build.VERSION.SDK_INT >= 11) {
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
                }
                dm.enqueue(request);
            }

            return true;
        }
        // if the download manager app has been disabled on the device
        catch (IllegalArgumentException e) {
            // show the settings screen where the user can enable the download manager app again
            openAppSettings(context, AdvancedWebView.PACKAGE_NAME_DOWNLOAD_MANAGER);

            return false;
        }
    }
*/
    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
            // Toast.makeText(MainActivity.this, "onDownloadRequested(url = "+url+",  suggestedFilename = "+suggestedFilename+",  mimeType = "+mimeType+",  contentLength = "+contentLength+",  contentDisposition = "+contentDisposition+",  userAgent = "+userAgent+")", Toast.LENGTH_LONG).show();

		if (AdvancedWebView.handleDownload(this, url, suggestedFilename)) {
			// download successfully handled
		}
		else {
			// download couldn't be handled because user has disabled download manager app on the device
		}
    }

    @Override
    public void onExternalPageRequest(String url) {
        //Toast.makeText(MainActivity.this, "onExternalPageRequest(url = "+url+")", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //duration = mPlayerAdapter.loadMedia(MEDIA_RES_ID);
       /* Long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        Long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration));
        String secondFormat;
        if(seconds<10)
        {
            secondFormat = "0" + seconds.toString();
        }
        else
            secondFormat = seconds + "";
        tvDuration.setText(minutes + ":" + secondFormat);
        Log.d(TAG, "onStart: create MediaPlayer");*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isChangingConfigurations() && mPlayerAdapter.isPlaying()) {
            Log.d(TAG, "onStop: don't release MediaPlayer as screen is rotating & playing");
        } else {
            mPlayerAdapter.release();
            Log.d(TAG, "onStop: release MediaPlayer");
        }
    }

    public void initializePlaybackController() {
        MediaPlayerHolder mMediaPlayerHolder = new MediaPlayerHolder(this);
        Log.d(TAG, "initializePlaybackController: created MediaPlayerHolder");
        mMediaPlayerHolder.setPlaybackInfoListener(new PlaybackListener());
        mPlayerAdapter = mMediaPlayerHolder;
        Log.d(TAG, "initializePlaybackController: MediaPlayerHolder progress callback set");
    }

    public class PlaybackListener extends PlaybackInfoListener {


        public void onDurationChanged(int duration) {
            mSeekbarAudio.setMax(duration);
            mSeekbarAudio_min.setMax(duration);
            Log.d(TAG, String.format("setPlaybackDuration: setMax(%d)", duration));
        }


        public void onPositionChanged(int position) {
            if (!mUserIsSeeking) {
               // mSeekbarAudio.setProgress(position, true);
               // mSeekbarAudio_min.setProgress(position, true);
                Log.d(TAG, String.format("setPlaybackPosition: setProgress(%d)", position));
            }
        }


        public void onStateChanged(@State int state) {
            String stateToString = PlaybackInfoListener.convertStateToString(state);
            onLogUpdated(String.format("onStateChanged(%s)", stateToString));
        }


        public void onPlaybackCompleted() {
            //mPauseBtn.setVisibility(View.GONE);
            //mPlayBtn.setVisibility(View.VISIBLE);
        }


        public void onLogUpdated(String message) {
            if (mTextDebug != null) {
                mTextDebug.append(message);
                mTextDebug.append("\n");
                // Moves the scrollContainer focus to the end.
                mScrollContainer.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                mScrollContainer.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
            }
        }
    }
}