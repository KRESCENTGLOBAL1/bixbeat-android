package com.bixbeat.android;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import im.delight.android.webview.AdvancedWebView;
import mediaplayer.PlayerAdapter;

public class BixWebAppInterface {
    MainActivity mContext;
    String urlToPlay = "";
    String imageToShow =  "";
    String songName = "";
    String artistName =  "";

    /** Instantiate the interface and set the context */
    BixWebAppInterface(MainActivity c, LinearLayout player) {
        mContext = c;
    }

    private void downloadFile(String url, File outputFile) {


        if(!outputFile.exists())
        {
            try {

                URL u = new URL(url);
                URLConnection conn = u.openConnection();
                int contentLength = conn.getContentLength();

                //Log.d("content length", contentLength + "");

                DataInputStream stream = new DataInputStream(u.openStream());

                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();

                DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
                fos.write(buffer);
                fos.flush();
                fos.close();
                Boolean success = outputFile.createNewFile();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
            Toast.makeText(mContext, "This Song is Already Downloaded", Toast.LENGTH_SHORT).show();

    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void onDownload(final String song) {


        try {
            JSONObject songDetails = new JSONObject(song);
            JSONObject songFile = new JSONObject(songDetails.getString("song"));
            String urlToDownload = songFile.getString("downloadPathAbsolute");


            //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            // File file = new File(path, "db.csv");
            File file = new File(mContext.getFilesDir(), songFile.getString("songName"));


            //Boolean success = file.createNewFile();
            downloadFile(urlToDownload,file);
        }
        catch (Exception e)
        {
            Log.d("exception","file not downloaded");
        }
    }

    @JavascriptInterface
    public void onMenuChanged(Boolean show) {
        /*if(show)
            mContext.player.setVisibility(View.VISIBLE);
        else
            mContext.player.setVisibility(View.GONE);*/
    }

    @JavascriptInterface
    public void onPlay(final String song) {

        try{
            JSONObject songDetails = new JSONObject(song);
            JSONObject songFile = new JSONObject(songDetails.getString("song"));
            urlToPlay = songFile.getString("absoluteAudioPath");
            imageToShow = songFile.getString("imageOriginalSrc");
            songName = songFile.getString("songName");
            artistName = songFile.getString("artistName");
            //Log.d("on play",songFile.getString("absoluteAudioPath"));
            //File file = new File(mContext.getFilesDir(), songName);


        }
        catch (JSONException e)
        {
            Log.d("json error", e.toString());
        }


        mContext.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                    mContext.mPlayerAdapter.pause();
                    mContext.mPlayerAdapter.release();

                String shortSongName;

                if(songName.length() > 20)
                    shortSongName = songName.substring(0,20) + "...";
                else
                    shortSongName = songName;
                mContext.minimized_player.setVisibility(View.VISIBLE);
                String url = urlToPlay; // your URL here
                mContext.mPlayerAdapter.loadMediaUrl(url);
                mContext.cover.setImageBitmap(BitmapFactory.decodeFile(imageToShow));
                //Picasso.get().load(imageToShow).into(mContext.cover);
                mContext.tvSongTitleMin.setText(shortSongName);
                mContext.tvArtistMin.setText(artistName);
                mContext.mPlayerAdapter.play();
                mContext.mPlayBtn_min.setVisibility(View.GONE);
                mContext.mPauseBtn_min.setVisibility(View.VISIBLE);

            }
        });
        //mPlayerAdapter.play();
    }


    @JavascriptInterface
    public void onLoginGoogle() {
        mContext.getAuthCode();
    }

    @JavascriptInterface
    public void onShare(String title, String text, String url) {
        mContext.shareLink(title, text, url);
        Toast.makeText(mContext, "test", Toast.LENGTH_SHORT).show();
    }
}

